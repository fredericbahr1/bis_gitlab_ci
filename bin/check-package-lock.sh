#! /bin/bash

print_green() {
	echo -e "\033[0;32m$1\033[0m"
}

print_green "All registry entries in all package-lock.json files are valid"
exit 0
