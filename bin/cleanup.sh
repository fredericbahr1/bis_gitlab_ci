#! /bin/bash

print_yellow() {
  echo -e "\033[1;33m$1\033[0m"
}

print_green() {
	echo -e "\033[0;32m$1\033[0m"
}

print_yellow "Cleaning up files\n"

rm -rf public

print_green "Folder public was removed."