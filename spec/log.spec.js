const { log } = require("../log.js");

describe("#runner", () => {
  let originalLogFunc;

  beforeEach(() => {
    originalLogFunc = console.log;
    console.log = jasmine.createSpy("log");
  });

  afterEach(() => {
    console.log = originalLogFunc;
    originalLogFunc = undefined;
  });

  it("should log something", () => {
    log();

    expect(console.log).toHaveBeenCalledTimes(1);
    expect(console.log).toHaveBeenCalledWith("Very complex Node-application");
  });
});
